FROM centos

# copy mongo repo config
ADD ./code/mongodb.repo /etc/yum.repos.d/mongodb.repo

# update repos
RUN yum -y update

# install mongo
RUN yum -y install mongodb-org mongodb-org-server

# add mongod config file
ADD ./code/mongod.conf /etc/mongod.conf

# port
EXPOSE 27017

# run mongo
ENTRYPOINT ["mongod", "-f", "/etc/mongod.conf"]
